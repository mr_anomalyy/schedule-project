export enum ResponseStatus {
  "OK",
  "ERR"
}

export interface IResponse {
  status: ResponseStatus
}

export interface IAuthResponse extends IResponse {
  token?: string;
}
