export type WidthClass = "xs" | "sm" | "md" | "lg" | "xl";
export type WidthClassWithFalse = WidthClass | false;
