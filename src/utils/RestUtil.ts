export type RequestMethodType = "GET" | "POST" | "DELETE" | "PUT";

class RestUtil {
  public static request(url: string, method: RequestMethodType, params: Map<string, string>) {
    return method === "GET" ?
      this.fetchGetMethod(url, params) :
      this.fetchWithParams(url, method, params);
  }

  protected static fetchGetMethod(url: string, params: Map<string, string>): Promise<Response> {
    return fetch(url + "?" + this.prepareParams(params));
  }

  protected static fetchWithParams(
    url: string,
    method: RequestMethodType,
    params: Map<string, string>
  ): Promise<Response> {
    return fetch(url, {
      method,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: this.prepareParams(params),
    });
  }

  protected static prepareParams(params: Map<any, any>): string {
    let paramsString = "";
    for (const [key, value] of params.entries()) {
      paramsString += key + "=" + value + "&";
    }
    if (paramsString.slice(-1) === "&") {
      paramsString = paramsString.slice(0, -1);
    }
    return paramsString;
  }

}

export default RestUtil;
