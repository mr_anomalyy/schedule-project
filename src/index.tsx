import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Router, Route, IndexRoute, hashHistory } from "react-router";
import "./index.css";
import App from "./App";
import { MainScreen, ElementEditorScreen } from "./screens";
import store from "./store";
import { I18nextProvider } from "react-i18next";
import i18n from "./i18n";
import Loader from "./Loader";

const Root = () => {
  return (
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <Router history={hashHistory}>
          <Route path="/" component={App}>
            <IndexRoute component={MainScreen} />
            <Route path="element-editor" component={ElementEditorScreen} />
          </Route>
        </Router>
      </Provider>
    </I18nextProvider>
  );
};
setTimeout(() => {
  ReactDOM.render(
    <Suspense fallback={<Loader />}>
      <Root />
    </Suspense>,
    document.getElementById("root")
  );
}, 100);
// @todo fix i18n other way
