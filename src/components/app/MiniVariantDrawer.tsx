import React from "react";
import clsx from "clsx";
import { withStyles, createStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";

import { DRAWER_WIDTH } from "../../constants";

const styles = (theme: any) => createStyles(({
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: DRAWER_WIDTH,
    flexShrink: 0,
  },
  drawerOpen: {
    width: DRAWER_WIDTH,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    padding: theme.spacing(3) as string,
    flexGrow: 1,
  },
}));

interface IMiniVariantDrawerProps {
  classes: any;
  open: boolean;
}

class MiniVariantDrawer extends React.Component<IMiniVariantDrawerProps, {}> {
  public render() {
    const { classes, open } = this.props;
    return (
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
        open={open}
      >
        {this.props.children}
      </Drawer>
    );
  }
}

export default withStyles(styles)(MiniVariantDrawer);
