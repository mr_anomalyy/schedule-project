import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Divider from "@material-ui/core/Divider";
import { CloseDrawerOption, MiniVariantDrawer } from "./";

export interface IDrawerProps {
  isDrawerOpen: boolean;
  state: "active" | "not-active" | "not-authorized"
  onDrawerClose: () => void;
}

interface IDrawerState { }

class Drawer extends React.Component<IDrawerProps, IDrawerState> {
  public render() {
    if (this.props.state === "not-authorized") {
      return null;
    }

    return (
      <MiniVariantDrawer
        open={this.props.isDrawerOpen}
      >
        {this.renderItems()}
      </MiniVariantDrawer>
    );
  }

  protected renderItems(): JSX.Element[] {
    return [
      this.renderCloseDrawerOption(),
      this.renderExitOption()
    ];
  }

  protected renderCloseDrawerOption(): JSX.Element {
    return (
      <div key="closeOption">
        <CloseDrawerOption onButtonClick={this.props.onDrawerClose} />
        <Divider />
      </div>
    );
  }

  protected renderExitOption(): JSX.Element {
    return (
      <ListItem key="exitOption" button>
        <ListItemIcon>
          <ExitToAppIcon />
        </ListItemIcon>
        <ListItemText primary={"Выход"} />
      </ListItem>
    );
  }

}

export default Drawer;
