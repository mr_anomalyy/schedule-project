import React from "react";
import clsx from "clsx";
import { withStyles, createStyles } from "@material-ui/core/styles";
import { AppBar, Toolbar, IconButton, Typography } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";

import { DRAWER_WIDTH, APP_FULL_NAME } from "@app/constants";
import { Trans } from "react-i18next";

const styles = (theme: any) => createStyles({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: DRAWER_WIDTH,
    width: `calc(100% - ${DRAWER_WIDTH}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
});


export interface ITopBarProps {
  classes?: any;
  isDrawerOpen: boolean;
  onDrawerButton: () => void;
  state: "active" | "not-active" | "not-authorized"
}

class TopBar extends React.Component<ITopBarProps, {}> {
  public render() {
    const { classes, isDrawerOpen } = this.props;
    return (
      <AppBar
        position={"fixed"}
        className={clsx(classes.appBar, {
          [classes.appBarShift]: isDrawerOpen,
        })}
      >
        <Toolbar>
          {this.renderDrawerButton()}
          <Typography color="inherit">
            <Trans i18nKey={"app_name"}>{APP_FULL_NAME}</Trans>
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }

  protected renderDrawerButton(): JSX.Element | null {
    return !this.props.isDrawerOpen ? (
      <IconButton color="inherit" aria-label="Menu" onClick={this.props.onDrawerButton}>
        <MenuIcon />
      </IconButton>
    ) : null;
  }

}

export default withStyles(styles)(TopBar);
