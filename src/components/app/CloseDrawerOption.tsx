import React from "react";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";

const styles = (theme: any) => ({
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
});

interface ICloseDrawerOptionProps {
  classes: any;
  onButtonClick: () => void;
}

class CloseDrawerOption extends React.Component<ICloseDrawerOptionProps, {}> {
  public render() {
    const { classes } = this.props;
    return (
      <div className={classes.toolbar}>
        <IconButton onClick={this.props.onButtonClick}>
          <ChevronLeftIcon/>
        </IconButton>
      </div>
    );
  }
}

export default withStyles(styles)(CloseDrawerOption);
