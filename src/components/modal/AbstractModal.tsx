import React from "react";
import Button, { ButtonProps } from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { WidthClassWithFalse } from "@app/types";
import { LinearProgress } from "@material-ui/core";

interface IModalAction {
  text: string;
  buttonProps?: ButtonProps;
}

export interface IAbstractModalProps {
  isOpen: boolean;
  maxWidth?: WidthClassWithFalse;
  actions?: IModalAction[];
  isLoading?: boolean;
}

abstract class AbstractModal<
  P extends IAbstractModalProps = IAbstractModalProps,
  S = {}
> extends React.Component<P, S> {
  protected abstract get title(): JSX.Element | string;

  protected get modalMaxWidth(): WidthClassWithFalse {
    const { maxWidth } = this.props;
    return maxWidth === undefined
      ? "md"
      : maxWidth!;
  }

  protected renderModalAction(action: IModalAction): JSX.Element {
    return (
      <Button {...action.buttonProps} >
        {action.text}
      </Button>
    );
  }

  protected renderModalActions(): JSX.Element | undefined {
    return this.props.actions ?
      (
        <DialogActions>
          {this.props.actions.map((action) => this.renderModalAction(action)) }
        </DialogActions>
      ) : undefined;
  }

  protected abstract renderContent(): JSX.Element | JSX.Element[] | undefined;

  public render() {
    return (
      <Dialog open={this.props.isOpen} maxWidth={this.modalMaxWidth} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{this.title}</DialogTitle>
        <DialogContent>
          {this.renderContent()}
        </DialogContent>
        {this.renderModalActions()}
        {this.props.isLoading ? <LinearProgress /> : null}
      </Dialog>
    );
  }
}

export default AbstractModal;
