import React from "react";
import { container } from "@app/ioc";
import { AbstractModal, IAbstractModalProps } from "@app/components/modal";
import { DialogContentText, TextField } from "@material-ui/core";
import { AuthorizationService } from "@app/services";
import { Trans, Translation } from "react-i18next";

interface IAuthModalProps extends IAbstractModalProps {
  callback?: () => void;
  loginFieldRef?: (ref: any) => void;
  passwordFieldRef?: (ref: any) => void;
  loginFieldError?: boolean;
  passwordFieldError?: boolean;
}

class AuthModal extends AbstractModal<IAuthModalProps> {

  protected authorizationService: AuthorizationService =
  container.get<AuthorizationService>(AuthorizationService);

  protected get title(): JSX.Element | string {
    return (
      <Translation>
        {
          (t) => t("auth.modal_title")
        }
      </Translation>
    );
  }

  protected renderContent(): JSX.Element | JSX.Element[] | undefined {
    return (
      <>
        <DialogContentText><Trans i18nKey={"auth.you_need_to_auth"} /></DialogContentText>
        <Translation>
          {
            (t) => (
              <>
                <TextField
                  autoFocus
                  margin="dense"
                  id="loginUser"
                  label={t("auth.username_label")}
                  disabled={this.props.isLoading}
                  type="text"
                  inputRef={this.props.loginFieldRef}
                  error={this.props.loginFieldError}
                  fullWidth
                />
                <TextField
                  margin="dense"
                  id="loginPassword"
                  label={t("auth.password_label")}
                  disabled={this.props.isLoading}
                  type="password"
                  inputRef={this.props.passwordFieldRef}
                  error={this.props.passwordFieldError}
                  fullWidth
                />
              </>
            )
          }
        </Translation>
      </>
    );
  }

}

export default AuthModal;
