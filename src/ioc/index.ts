import { Container } from "inversify";
import { CookieService, AuthorizationService } from "@app/services";

const container = new Container();

container.bind<CookieService>(CookieService).to(CookieService);
container.bind<AuthorizationService>(AuthorizationService).to(AuthorizationService);

export { container };
