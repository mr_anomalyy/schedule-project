import { SET_AUTHORIZED_STATUS, RESET_AUTHORIZED_STATUS } from "@app/constants";
import { Action } from "redux";

const initialState = {
  isAuthorized: false,
};

export default function AppStateReducer(state = initialState, action: Action) {
  switch (action.type) {
    case SET_AUTHORIZED_STATUS:
      return {
        ...state,
        isAuthorized: true,
      };
    case RESET_AUTHORIZED_STATUS:
      return {
        ...state,
        isAuthorized: false,
      };
    default:
      return state;
  }
};
