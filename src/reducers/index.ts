import { combineReducers } from "redux";
import appStateReducer from "./AppState";

const rootReducer = combineReducers({
  appStateReducer,
});

export default rootReducer;
