import { injectable } from "inversify";
import Cookies from "js-cookie";

@injectable()
class CookieService {

  public setCookie(name: string, value: string, params?: Cookies.CookieAttributes): void {
    Cookies.set(name, value, params);
  }

  public getCookie(name: string): string | undefined {
    return Cookies.get(name);
  }

  public removeCookie(name: string, params?: Cookies.CookieAttributes): void {
    Cookies.remove(name, params);
  }

}

export type CookieServiceType = typeof CookieService;
export default CookieService;
