import { injectable } from "inversify";
import { Md5 } from "ts-md5";
import { container } from "@app/ioc";
import { CookieService } from "@app/services";
import { AUTH_ENDPOINT } from "@app/constants";
import { RestUtil } from "@app/utils";
import { ResponseStatus, IAuthResponse } from "@app/types";

@injectable()
class AuthorizationService {
  protected cookieService: CookieService = container.get<CookieService>(CookieService);
  protected authStatus: boolean = false;

  constructor() {
    this.initialize();
  }

  protected initialize() {
    const userToken = this.cookieService.getCookie("userToken");
    if (!userToken) {

    }
  }

  public async authorizationWithCredentials(login: string, password: string): Promise<boolean> {
    if (!login || !password) {
      return false;
    }

    const resp: IAuthResponse = await this.authRequest(login, this.md5(password))
      .then((r) => r.json());

    if (resp.status === ResponseStatus.OK) {
      this.cookieService.setCookie("token", resp.token!);
      this.authStatus = true;
      return true;
    }

    return false;
  }

  protected md5(_string: string) {
    return Md5.hashStr(_string) as string;
  }

  protected authRequest(login: string, password: string) {
    return RestUtil.request(AUTH_ENDPOINT, "POST", new Map([["login", login], ["password", password]]));
  }

  public get isAuthorized(): boolean {
    return this.authStatus;
  }

}

export type AuthorizationServiceType = typeof AuthorizationService;
export default AuthorizationService;
