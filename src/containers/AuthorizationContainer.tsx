import React from "react";
import { container } from "@app/ioc";
import { AuthModal } from "@app/components/modal";
import { AuthorizationService } from "@app/services";
import { Translation } from "react-i18next";


export interface IAuthorizationContainerProps { }

export interface IAuthorizationContainerState {
  forgotPasswordModalIsOpen: boolean;
  loading: boolean;
  loginFieldError: boolean;
  passwordFieldError: boolean;
}

class AuthorizationContainer extends React.Component<IAuthorizationContainerProps, IAuthorizationContainerState> {
  protected authorizationService: AuthorizationService = container.get<AuthorizationService>(AuthorizationService);

  protected loginFieldRef: HTMLInputElement | null = null;
  protected passwordFieldRef: HTMLInputElement | null = null;

  constructor(props: IAuthorizationContainerProps) {
    super(props);

    this.handleLoginButton = this.handleLoginButton.bind(this);
    this.handleForgotPasswordButton = this.handleForgotPasswordButton.bind(this);
  }

  public state: IAuthorizationContainerState = {
    forgotPasswordModalIsOpen: false,
    loading: false,
    loginFieldError: false,
    passwordFieldError: false,
  }

  protected async handleLoginButton() {
    this.setState({ loading: true });

    if (!this.loginFieldRef || !this.passwordFieldRef) {
      return;
    }
    this.setState({ loginFieldError: false, passwordFieldError: false });

    const loginVal = this.loginFieldRef.value;
    const passwordVal = this.passwordFieldRef.value;

    if (!loginVal || !passwordVal) {
      let errorState = {};
      if (!loginVal) {
        errorState = { ...errorState, loginFieldError: true };
      }
      if (!passwordVal) {
        errorState = { ...errorState, passwordFieldError: true };
      }
      this.setState(errorState);
      this.setState({ loading: false });
      return;
    }

    if (await this.authorizationService.authorizationWithCredentials(loginVal, passwordVal)) {
      setTimeout(() => this.setState({ loading: false }), 1500);
      return;
    }
  }

  protected handleForgotPasswordButton() {
    return alert("Данная функция пока что недоступна");
  }

  public render() {
    return (
      <Translation>
        {
          (t) =>
            <AuthModal
              isOpen={true}
              isLoading={this.state.loading}
              loginFieldRef={(ref) => { this.loginFieldRef = ref; }}
              passwordFieldRef={(ref) => { this.passwordFieldRef = ref; }}
              loginFieldError={this.state.loginFieldError}
              passwordFieldError={this.state.passwordFieldError}
              actions={[
                { text: t("auth.login_button"), buttonProps: { onClick: this.handleLoginButton } },
                { text: t("auth.forgot_password_button?"), buttonProps: { onClick: this.handleForgotPasswordButton } }
              ]}
            />
        }
      </Translation>
    );
  }
}
export default AuthorizationContainer;
