import React from "react";
import { TopBar, ITopBarProps, Drawer, IDrawerProps } from "@app/components/app";

interface IUserUIContainerProps extends IDrawerProps, ITopBarProps { }

class UserUIContainer extends React.Component<IUserUIContainerProps, {}> {
  public render() {
    return (
      <>
        <TopBar
          onDrawerButton={this.props.onDrawerButton}
          state={this.props.state}
          isDrawerOpen={this.props.isDrawerOpen}
        />
        <Drawer
          onDrawerClose={this.props.onDrawerClose}
          state={this.props.state}
          isDrawerOpen={this.props.isDrawerOpen}
        />
      </>
    );
  }
}
export default UserUIContainer;
