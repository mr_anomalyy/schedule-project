import React from "react";
import "reflect-metadata";
import { container } from "@app/ioc";
import { AuthorizationService } from "@app/services";
import { UserUIContainer, AuthorizationContainer } from "@app/containers";

interface IAppState {
  isDrawerOpen: boolean;
}

class App extends React.Component<{}, IAppState> {
  protected authorizationService: AuthorizationService = container.get<AuthorizationService>(AuthorizationService);

  constructor(props: {}) {
    super(props);
    this.handleOnDrawerButton = this.handleOnDrawerButton.bind(this);
    this.handleOnDrawerClose = this.handleOnDrawerClose.bind(this);
    this.getAppState = this.getAppState.bind(this);
  }

  public state: IAppState = {
    isDrawerOpen: false,
  }

  protected getAppState(): "active" | "not-active" | "not-authorized" {
    if (!this.authorizationService.isAuthorized) {
      return "not-authorized";
    } else {
      return "active";
    }
  }

  protected handleOnDrawerButton(): void {
    if (this.getAppState() === "active") {
      this.setState({ isDrawerOpen: !this.state.isDrawerOpen });
    }
  }

  protected handleOnDrawerClose(): void {
    this.setState({ isDrawerOpen: false });
  }

  protected renderAuthorization(): JSX.Element | undefined {
    return this.getAppState() === "not-authorized" ? (
      <AuthorizationContainer />
    ) : undefined;
  }

  protected renderUI(): JSX.Element {
    return (
      <UserUIContainer
        isDrawerOpen={this.state.isDrawerOpen}
        state={this.getAppState()}
        onDrawerClose={this.handleOnDrawerClose}
        onDrawerButton={this.handleOnDrawerButton}
      />
    );
  }

  public render() {
    return (
      <>
        {this.renderAuthorization()}
        {this.renderUI()}
        <div>{this.props.children}</div>
      </>
    );
  }
}

export default App;
