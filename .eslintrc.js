const path = require('path');

module.exports = {
  "env": {
    "browser": true,
    "es6": true
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "project": path.resolve(__dirname, './tsconfig.json'),
    "sourceType": "module",
    "tsconfigRootDir": __dirname
  },
  "plugins": ["@typescript-eslint", "eslint-plugin-react"],
  "rules": {
    "@typescript-eslint/adjacent-overload-signatures": "error",
    "@typescript-eslint/array-type": "error",
    "@typescript-eslint/ban-types": "error",
    "@typescript-eslint/class-name-casing": "error",
    "@typescript-eslint/explicit-member-accessibility": ["error", {
      "overrides": {
        "constructors": "off"
      }
    }],
    "@typescript-eslint/indent": ["warn", 2],
    "@typescript-eslint/no-empty-interface": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-misused-new": "error",
    "@typescript-eslint/no-namespace": "error",
    "@typescript-eslint/no-parameter-properties": "off",
    "@typescript-eslint/no-use-before-declare": "off",
    "@typescript-eslint/no-var-requires": "error",
    "@typescript-eslint/prefer-for-of": "error",
    "@typescript-eslint/prefer-function-type": "error",
    "@typescript-eslint/prefer-namespace-keyword": "error",
    "@typescript-eslint/type-annotation-spacing": "error",
    "@typescript-eslint/unified-signatures": "error",
    "arrow-parens": ["error", "always"],
    "comma-dangle": ["warn",
      {
        "arrays": "never",
        "objects": "always-multiline",
        "imports": "only-multiline",
        "exports": "only-multiline",
        "functions": "never"
      }
    ],
    "complexity": "off",
    "constructor-super": "error",
    "curly": "error",
    "dot-notation": "error",
    "eol-last": "error",
    "eqeqeq": ["warn", "smart"],
    "guard-for-in": "error",
    "indent": "off",
    "keyword-spacing": "error",
    "max-classes-per-file": ["error", 5],
    "max-len": ["error", {
      "code": 120,
      "tabWidth": 2
    }],
    "new-parens": "error",
    "no-bitwise": "off",
    "no-caller": "error",
    "no-cond-assign": "error",
    "no-console": ["error", {
      "allow": ["log"]
    }],
    "no-console": "warn",
    "no-debugger": "error",
    "no-dupe-keys": "warn",
    "no-duplicate-imports": "warn",
    "no-empty": "off",
    "no-fallthrough": "off",
    "no-irregular-whitespace": ["warn", {
      "skipComments": true,
      "skipRegExps": true,
      "skipStrings": true,
      "skipTemplates": true
    }],
    "no-multiple-empty-lines": "error",
    "no-new-wrappers": "error",
    "no-throw-literal": "error",
    "no-trailing-spaces": "error",
    "no-trailing-spaces": "warn",
    "no-undef-init": "error",
    "no-unsafe-finally": "error",
    "no-unused-expressions": "error",
    "no-unused-labels": "error",
    "no-unused-vars": "error",
    "@typescript-eslint/no-unused-vars": ["error", 
      {
        "vars": "all",
        "args": "after-used",
        "ignoreRestSiblings": false
      }
    ],
    "react/jsx-uses-react": "error",
    "react/jsx-uses-vars": "error",
    "no-var": "error",
    "no-whitespace-before-property": "warn",
    "object-shorthand": "error",
    "one-var": "off",
    "prefer-const": "error",
    "quote-props": ["error", "consistent-as-needed"],
    "quotes": ["error", "double"],
    "quotes": ["warn", "double"],
    "radix": "error",
    "semi": ["warn", "always"],
    "some-rule": "off",
    "space-before-function-paren": ["error", {
      "anonymous": "never",
      "asyncArrow": "always",
      "named": "never"
    }],
    "spaced-comment": ["error", "always"],
    "use-isnan": "error",
    "valid-typeof": "off"
  }
};